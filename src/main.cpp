/*
IR remote for Samsung TV based on Attiny 84

Initial CPU freq is supposed to be 1 MHz
Brown-out detector and Watchdog are disabled by fuses
*/


#include "ir_codes.h"
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>

#define LED_DDR   DDRA
#define LED_PORT  PORTA
#define LED_PIN   PA7


void pulse(char ch) {
    for (int i = 0; i < 22; i++) {
        setBit(LED_PORT, LED_PIN);
        _delay_us(pulsePwm);
        clearBit(LED_PORT, LED_PIN);
        _delay_us(waitPwm);
    }

    if (ch == '0') {
        clearBit(LED_PORT, LED_PIN);
        _delay_us(350); // effective delay 560 uS

    } else {
        clearBit(LED_PORT, LED_PIN);
        _delay_us(1450); // effective delay 1680 uS
    }
}

void sendButton(const char *button) {
    for (int i = 0; i < 170; i++) { // start bit
        setBit(LED_PORT, LED_PIN);
        _delay_us(pulsePwm);
        clearBit(LED_PORT, LED_PIN);
        _delay_us(waitPwm);
    }
    // bitSet(LED_PORT, LED_PIN);
    // delayMicroseconds(4500);
    clearBit(LED_PORT, LED_PIN);
    _delay_us(4300); // effective delay 4500 uS

    for (unsigned int i = 0; i < strlen(button); i++) {
        pulse(button[i]);
    }

    pulse('0'); // stop bit
}

ISR(PCINT0_vect) {
    _delay_ms(50); // debounce

    if (bit_is_clear(PINA, PA6)) {
        sendButton(RIGHT);
    
    } else if (bit_is_clear(PINA, PA5)) {
        sendButton(DOWN);
    
    } else if (bit_is_clear(PINA, PA4)) {
        sendButton(UP);
    
    } else if (bit_is_clear(PINA, PA3)) {
        sendButton(ENTER);

    } else if(bit_is_clear(PINA, PA2)) {
        sendButton(LEFT);

    } else if(bit_is_clear(PINA, PA1)) {
        sendButton(BACK);

    } else if(bit_is_clear(PINA, PA0)) {
        sendButton(POWER);

    }
}

ISR(PCINT1_vect) {
    _delay_ms(50); // debounce

    if (bit_is_clear(PINB, PB0)) {
        sendButton(VOL_DOWN);

    } else if(bit_is_clear(PINB, PB1)) {
        sendButton(VOL_UP);

    } else if(bit_is_clear(PINB, PB2)) {
        sendButton(SOURCE);
    }
}

void cpu_powerDown()
{
    // enabling sleep + power down mode
    MCUCR = SLEEP_MODE_PWR_DOWN | (1 << SE);
    asm("SLEEP");
}

int main() {
    // Initial CPU speed is 1 MHz, so we don't change it
    // // Set TINY84 frequency to 1 MHz
    // CLKPR = 0x80;   // write the CLKPCE bit to one and all the other to zero
    // CLKPR = _BV(CLKPS0) | _BV(CLKPS1); // write the CLKPS0..3 bits while writing the CLKPE bit to zero 

    LED_DDR |= _BV(LED_PIN); // LED pin output

    // one more pin is available
    // if (bit_is_set(MCUSR, EXTRF)) { // reset pin handler
    //     clearBit(MCUSR, EXTRF);
    //     sendButton(POWER);
    // }

    PORTA = 0b01111111; // pullups for all PCINT except PA7 (LED)
    PORTB = 0b0111;     // and PB3 (RESET)
    PCMSK0 = 0b01111111; // pinchange interrupts on PCINT 0-6
    PCMSK1 = 0b0111;     // PCINT 8-10
    GIMSK |= _BV(PCIE0) | _BV(PCIE1); // enable interrupts on key press
    sei();

    while (true) {
        cpu_powerDown();
    }

}