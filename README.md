# IR remote for Samsung TV
I made it resolve issues with my smart TV which had only bluetooth remote which died after 2 years.
This is a known problem. Many models of Samsung smart TVs year 2017-2018 affected. To resolve the issue you'll have to buy a new Wifi/BT module and replace a fauly one which is a bit risky. Or just use this IR remote.

IR remote should work with both modern smart and older non-smart TVs although I only tested it on my Ue49mu6400

### Details
Here I use microcontroller *Attiny84* from Microchip (ex Atmel). But Attiny24/44 can be used as well. I tune it to 1 MHz (8MHz internal RC-resonator with prescaler of 8) so it can function at very low voltage given by pretty dead alcoline battery.

As a drawback time delays are not precise. Actual delays are longer than you set. Check them with oscillograph or logical analizer.

### PCB

Check it here: https://easyeda.com/gta2k/TV-remote